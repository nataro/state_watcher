import time
from pathlib import Path, WindowsPath, PosixPath
from typing import Sequence, Optional, Dict

import cv2
import numpy as np

import fastai
from fastai.data.all import *
from fastai.vision.all import *

"""
fastai v2 用
"""

'''
fastaiで保存したモデル（*.pkl）を読み込む前に、このモデルを生成した際に定義しておいた
自作クラスを、ここでも同様に定義しておかないとエラーとなる。

今回は、AlbumentationsTransformがそれに相当する
'''


def cv2fastpil(image):
    ''' OpenCV型 -> fastai.vision.core.PILImage型 '''
    new_image = image.copy()
    if new_image.ndim == 2:  # モノクロ
        pass
    elif new_image.shape[2] == 3:  # カラー
        new_image = new_image[:, :, ::-1]
    elif new_image.shape[2] == 4:  # 透過
        new_image = new_image[:, :, [2, 1, 0, 3]]

    # fastai.vision.core.PILImage.create()で<class 'fastai.vision.core.PILImage'>を作る
    new_image = PILImage.create(new_image)
    return new_image


class AlbumentationsTransform(RandTransform):
    "A transform handler for multiple `Albumentation` transforms"

    split_idx = None  # train,valid両方に適用させる
    order = 2  # 処理の優先順位（小さいほど高い？）

    def __init__(
            self,
            train_aug,  # albumentations.Composeでまとめたtrain用の処理を渡す
            valid_aug  # albumentations.Composeでまとめたtvalid用の処理を渡す
    ):
        store_attr()

    def before_call(self, b, split_idx):
        # これでtrainとvalidの分岐判断用のself.idxが自動取得できる？
        self.idx = split_idx

    def encodes(self, img: PILImage):
        if self.idx == 0:
            aug_img = self.train_aug(image=np.array(img))['image']
        else:
            aug_img = self.valid_aug(image=np.array(img))['image']
        return PILImage.create(aug_img)


class Fai2Classifier(object):
    # def __init__(self, path: str, file_name: str, size: int):
    def __init__(self, path: str, size: int):
        self._path = Path(path)
        # self._file_name = file_name  # 読み込むファイル名「*.pkl
        self._size = size  # 入力画像 size x size
        # self._learn = fastai.learner.load_learner(self._path / self._file_name, cpu=True)  # 古いバージョンには無い
        self._learn = fastai.learner.load_learner(self._path, cpu=True)  # 古いバージョンには無い
        # print(type(self._learn))

    def __call__(self, img: np.ndarray, *args, **kwargs):
        """
        :param img:  opencvで読み込んだ画像データを想定
        :param args:
        :param kwargs:
        :return:
        """

        # start = time.time()

        pred_class = self.predict(img)

        # process_time = time.time() - start
        # print(process_time)
        # print(pred_class)

        return pred_class

    def predict(self, img: np.ndarray):
        """
        :param img: opencvで読み込んだ画像データを想定
        :return: 各クラスの確率が格納された配列
        """
        img = cv2.resize(img, (self._size, self._size))
        fastpil_img = cv2fastpil(img)

        start = time.time()
        pred_class, pred_idx, outputs = self._learn.predict(fastpil_img)
        process_time = time.time() - start

        print(process_time)
        print(pred_class)

        return pred_class


def main():
    # path = 'ml'
    path = './ml/trained_model.pkl'
    # model_model = 'trained_model.pkl'
    # size = 224
    size = 128

    net = Fai2Classifier(
        path=path,
        # file_name=model_model,
        size=size
    )

    # opencvで画像を読み込む
    test_img = 'ml/tes_img/processing_00.jpg'
    img = cv2.imread(test_img)
    img = cv2.resize(img, (size, size))

    # モデルでクラスを予測
    pred_class = net(img)


if __name__ == "__main__":
    import warnings
    warnings.filterwarnings("ignore", category=UserWarning)

    main()
