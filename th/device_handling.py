import logging
import threading
from datetime import datetime
from abc import ABCMeta, abstractmethod
import json
from typing import Sequence, Optional, Dict
from pathlib import Path

import numpy as np
import cv2

from dev.callable_device import CallableDevice
from dev.xbee_router import XbeeSenderDevice
from dev.cam_capture import CaptureDevice
from utils.ar_clipper import ArClipper

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class DeviceHandlingBuilder(metaclass=ABCMeta):
    """
    ここを参考に
    https://qiita.com/__init__/items/74b36eba31ccbc0364ed
    """

    def __init__(self, device: CallableDevice):
        self._device = device
        self._event = threading.Event()  # 計測処理に関するスレッドを同期して開始するために使用
        self._lock = threading.Lock()  # 複数のスレッドが同時に録音デバイスにアクセスしないようにロックする

    def device_open(self) -> bool:
        return self._device.open()

    def device_close(self) -> None:
        self._device.close()

    def handling_thread(self, **kwargs):
        handling_t = threading.Thread(
            name=self.__class__.__name__ + '-handling_thread',
            target=self.handle_via_device,
            args=(self._event, self._lock,),
            kwargs=kwargs
        )
        handling_t.start()
        # 複数種類のスレッドを同時に実行させたいことがあるかもしれないので、event.set()でタイミングをあわせる予定
        self._event.set()  # event.wait()で待機しているプロセスが全部同時に解除される

    @abstractmethod
    def handle_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          **kwargs, ):
        pass


class DeviceHandlingBuilderReceive(DeviceHandlingBuilder):
    """
    thredを実行する際に辞書型などで情報を渡して、その情報をthred処理内で利用する場合、
    受け取った辞書型の内容についてバリデーションを行うための関数を追加したもの。
    """

    @abstractmethod
    def check_dict(self, received: Dict) -> str:
        pass

    @staticmethod
    def keys_check(target_keys: Sequence, must_keys: Sequence) -> bool:
        """
        target_keysの中に、must_keysが全部含まれているか確認する
        """
        return set(target_keys) >= set(must_keys)


