import logging
import threading
from datetime import datetime
from pathlib import Path
from typing import Sequence, Optional, Dict

import numpy as np

import cv2

from utils.ar_clipper import ArClipper
from ml.dl_model import Fai2Classifier, AlbumentationsTransform

from th.device_handling import DeviceHandlingBuilder
from dev.cam_capture import CaptureDevice

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class CamHandlingBuilder(DeviceHandlingBuilder):
    """
    usbカメラ等を用いて画像を撮影する

        * 取得した画像をマーカを基準として指定した範囲で切り取る
            * 切り取りのツールはutils.ar_clipper.ArClipperを用いる
                ArClipperには、切り取りマーカーや切り取り範囲の設定などが含まれる

        * 【未実装】ここにXbeeHandlingDirectorのインスタンスを持たせれば、解析結果をxbeeで送信できるのでは？
    """

    def __init__(self, device: CaptureDevice, ar_clipper=ArClipper()):
        super().__init__(device)
        self._ar_clipper = ar_clipper
        model_path = '/home/seisan/prg/image_capture2/ml/trained_model.pkl'
        # self._classifier = Fai2Classifier(path=model_path, size=128)
        self._classifier = None

    def handle_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          dir_path: str = '',
                          ext: str = 'jpg'):
        """
        DeviceHandlingBuilder.hadling_thred(self, **kwargs)において以下のように呼び出される
            handling_t = threading.Thread(
                name=self.__class__.__name__ + '-handling_thread',
                target=self.handle_via_device,
                args=(self._event, self._lock,),
                kwargs=kwargs
            )
            handling_t.start()
        """
        logging.debug(self.__class__.__name__ + ' : thread start')
        event.wait()  # event.set()が実行されるまで待機
        logging.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logging.debug(self.__class__.__name__ + ' : lock')

            print(dir_path, ext)

            # ここで画像取得
            data = self._device()
            # armarkerでクリップする
            data = self.ar_clipped_image(data)
            # 矩形画像の場合、正方形にpaddingする
            data = self.square_padding(data)
            if data is not None:
                print('classified')
                # # クラス分類結果を文字列で得る
                # c_name = self._classifier(data)
                # # ファイル名にクラス分離結果を追加して保存する
                # self.classified_image_writer(data, c_name, dir_path, ext)

        logging.debug(self.__class__.__name__ + ' : unlock')

    def ar_clipped_image(self, img: Optional[np.ndarray]) -> Optional[np.ndarray]:
        """
        utils.ar_clipper.ArClipperでマーカを基準として指定した範囲での切り取りを行う
        ArClipperには、切り取りマーカーや切り取り範囲の設定などが含まれる
        """
        if img is None: return None
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # マーカーボードを検出し、そのrotation vectorとtranslation vectorを返す
        retval, rvec_org, tvec_org = self._ar_clipper.estimate_board(gray)

        if retval != 0:  #
            # clippingする範囲の画像座標（ROI）を取得する
            roi = self._ar_clipper.roi_coordinates(rvec_org, tvec_org)

            # 画像を切り取る
            clipped_img = self._ar_clipper.image_clipping(img, roi)

            return clipped_img

        return None

    def classified_image_writer(self,
                                img: np.ndarray,
                                class_name: str,
                                dir_path: str,
                                ext: str = 'jpg',
                                limit_img_num: int = 50):
        """
        画像データと分類したクラス名を渡して、そのクラス名をファイル名に追加して画像データを保存する
        保存するディレクトリ内に同じクラスのファイルがlimit_img_num以上ある場合、それ以上画像を保存しない
        """
        if img is None: return

        # 　ファイル名の先頭にクラス名を追加
        dt = datetime.now()
        file_path = dir_path + "/{0}_{1}.".format(class_name, dt.strftime("%Y%m%d-%H%M%S-%f")) + ext

        # ディレクトリ内に同じクラス名のファイルが何個あるか確認する
        file_num = self.get_files_num(dir_path, str(class_name))

        # 同じクラスのファイルは個数を確認して保存するかどうか判断する
        if file_num <= limit_img_num:
            cv2.imwrite(file_path, img)

    @staticmethod
    def get_files_num(dir_path, file_name: str) -> int:
        """
        ディレクトリに含まれるファイルの内、ファイル名の一部が一致するファイルの個数をカウントする
        Parameters
        ----------
        dir_path:検索対象となるディレクトリ
        file_name:検索するファイル名の一部

        Returns
        -------
        該当するファイルの個数
        """
        path_ob = Path(dir_path)
        file_name_list = [str(p) for p in path_ob.glob('*')]
        # 取得した各ファイル名を指定した文字でスプリットし、その要素数をカウントしていく
        # 該当する文字が含まれるファイル名は2以上となり、該当しない場合は1となる
        sep_cont_list = [len(f.split(file_name)) for f in file_name_list]
        return sum(sep_cont_list) - len(file_name_list)

    @staticmethod
    def square_padding(img: Optional[np.ndarray]) -> Optional[np.ndarray]:
        """
        正方形の画像にパディングする
        Parameters
        ----------
        img

        Returns
        -------
        """
        if img is None: return None
        mean = (0.485, 0.456, 0.406)
        value = [int(i * 255) for i in mean]
        h, w, *c = img.shape
        sq_size = max([h, w])
        top = (sq_size - h) // 2
        bottom = sq_size - h - top
        left = (sq_size - w) // 2
        right = sq_size - w - left

        sq_img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=value)

        return sq_img


class CamHandlingDirector(object):
    def __init__(self, builder: CamHandlingBuilder, dir_path: str = None):
        self._builder = builder
        self._storage_path = self.create_storage_directory(dir_path)

    def __call__(self, *args, **kwargs):
        """
        __call__()でメインの動作ができるようにしておく
        """
        logger.debug(self.__class__.__name__ + ': __call__() called : ')
        self.handling()

    def handling(self) -> None:
        self._builder.handling_thread(dir_path=self._storage_path)

    def device_open(self) -> bool:
        return self._builder.device_open()

    def device_close(self) -> None:
        self._builder.device_close()

    @staticmethod
    def create_storage_directory(dir_path: str = None) -> str:
        """
        インスタンスを生成した時点での/年/月/日という階層のディレクトリを生成する
        Parameters
        ----------
        dir_path : str
            配置するディレクトリ
            （指定しない場合はカレントディレクトリになる）

        Returns
        -------
        str
            生成したディレクトリのパス
            （配置するディレクトリは自動生成される）

        """
        p_dir_path = Path(dir_path) if dir_path else Path.cwd()

        dt = datetime.now()
        y = dt.strftime('%Y')
        m = dt.strftime('%m')
        d = dt.strftime('%d')

        p_dir_path = p_dir_path / y / m / d

        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に生成
        p_dir_path.mkdir(exist_ok=True, parents=True)

        return str(p_dir_path.resolve())


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    import time

    cap_dev = CaptureDevice(cam_num=0)
    cap_hd = CamHandlingBuilder(cap_dev)
    # dir_path = '/mnt/hdd/satahdd'
    # cap_dc = CamHandlingDirector(cap_hd,dir_path=dir_path)
    cap_dc = CamHandlingDirector(cap_hd)

    print('open')
    cap_dc.device_open()

    time.sleep(1)

    cap_dc()

    time.sleep(10)  # 計測が実行される前にcloseするとエラーになるので待機

    cap_dc.device_close()

    print('closed')
