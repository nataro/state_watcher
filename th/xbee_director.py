import json
from typing import Optional, Dict

import logging
import threading

from th.device_handling import DeviceHandlingBuilder
from dev.xbee_router import XbeeSenderDevice

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class XbeeHandlingBuilder(DeviceHandlingBuilder):
    def __init__(self, device: XbeeSenderDevice):
        super().__init__(device)

    def handle_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          received: dict = None, ):
        """
        DeviceHandlingBuilder.hadling_thred(self, **kwargs)において以下のように呼び出される
            handling_t = threading.Thread(
                name=self.__class__.__name__ + '-handling_thread',
                target=self.handle_via_device,
                args=(self._event, self._lock,),
                kwargs=kwargs
            )
            handling_t.start()
        """
        logging.debug(self.__class__.__name__ + ' : thread start')
        event.wait()  # event.set()が実行されるまで待機
        logging.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logging.debug(self.__class__.__name__ + ' : lock')
            self._device(json.dumps(received))

        logging.debug(self.__class__.__name__ + ' : unlock')


class XbeeHandlingDirector(object):
    def __init__(self, builder: XbeeHandlingBuilder):
        self._builder = builder

    def __call__(self, *args, **kwargs):
        """
        __call__()でメインの動作ができるようにしておく
        """
        (received_dict,) = args
        # if not any(received_dict):
        if any(received_dict):  # dictが空かどうかのチェック
            # print(received_dict)
            logger.debug(self.__class__.__name__ + ': __call__() received : ', received_dict)
            self.handling(received_dict)

    def handling(self, received: Optional[Dict] = None) -> None:
        self._builder.handling_thread(received=received)

    def device_open(self) -> bool:
        return self._builder.device_open()

    def device_close(self) -> None:
        self._builder.device_close()


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。
    
    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package
    
    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager
    
    参考
    https://note.nkmk.me/python-relative-import/
    """

    import time

    PORT = 'COM4'
    BAUD = 9600

    xb_dev = XbeeSenderDevice(PORT, BAUD)
    xb_hb = XbeeHandlingBuilder(xb_dev)
    xb_dc = XbeeHandlingDirector(xb_hb)

    print('open')
    xb_dc.device_open()

    time.sleep(1)

    rec_dict = {'CC': 10.0, }
    print('send:', rec_dict)
    xb_dc(rec_dict)

    time.sleep(1)

    xb_dc.device_close()

    print('closed')
