import csv
from typing import Sequence, Union

import cv2
import numpy as np


def load_parameter(file_name):
    """
    csvデータを読み込んで文字列をfloatに変換して配列に格納したものを返す
    :param file_name:
    :return:
    """
    with open(file_name) as f:
        reader = csv.reader(f)
        return [[float(s) for s in row] for row in reader]


def transPos(trans_mat, target_pos):
    """
    透視投影の座標変換を行う。具体的な数値の遷移は以下参照。

    trans_mat:
    [ 3.15652152e+00 -7.16209823e-01 -1.66453805e+03]
    [-3.08903449e-01  3.93942752e+00 -1.40031748e+03]
    [-4.23401823e-04  5.10348019e-04  1.00000000e+00]

    target_pos:
    [516.5 409.5]

    np.append(target_pos,1):
    [516.5 409.5   1. ]

    trans_mat@target_pos:
    [-327.48260978   53.32945487    0.99030047]

    target_pos_trans[2]:
    0.9903004725337483

    target_pos_trans/target_pos_trans[2]:
    [-330.69014795   53.85179181    1.        ]

    """
    target_pos = np.append(target_pos, 1)
    target_pos_trans = trans_mat @ target_pos
    target_pos_trans = target_pos_trans / target_pos_trans[2]
    return target_pos_trans[:2]


def camera2screen(camera_mat, world_coord):
    """
    カメラ座標系の3次元座標を画像座標系の2次元座標に変換
    キャリブレーションで求めた3x3のカメラマトリックスを使用する

    camera_mat:
    [[1.33862088e+03, 0.00000000e+00, 9.85690260e+02],
     [0.00000000e+00, 1.39737705e+03, 4.92773839e+02],
    [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]]

    world_coord:
    [0.1, 0.2, 0.5 ]

    image_pos = camera_mat@world_coord:
    [6.26707218e+02 5.25862329e+02 5.00000000e-01]

    image_pos/image_pos[2]:
    [1.25341444e+03 1.05172466e+03 1.00000000e+00]

    """
    image_pos = camera_mat @ world_coord
    image_pos = image_pos / image_pos[2]
    return image_pos[:2]


class ArClipper(object):
    def __init__(self,
                 # coordinate=(-0.02, -0.65), width=0.45, height=0.45,
                 coordinate=(-0.02, -0.65), width=0.25, height=0.45,
                 # image_size=(224, 224),
                 image_size=224,  # specify width size
                 cam_matrix_file='./utils/mtx.csv',
                 cam_distortion_file='./utils/dist.csv',
                 # cam_matrix_file='/home/seisan/prg/image_capture2/utils/mtx.csv',
                 # cam_distortion_file='/home/seisan/prg/image_capture2/utils/dist.csv',
                 ):
        """

        :param coordinate:  検出したARマーカーを原点とし、どの座標を切り抜くか（m指定）
        :param width: 切り取る画像の幅（m指定）
        :param height: 切り取る画像の高さ（m指定）
        :param image_size: width の実サイズのみ指定するようにする
        :param cam_matrix_file:
        :param cam_distortion_file:
        """
        z = 0
        x, y = coordinate
        w, h = width, height
        # 切り取る範囲の座標情報（m単位）
        self._clipping_area = [
            [[x], [y], [z]],
            [[x + w], [y], [z]],
            [[x], [y + h], [z]],
            [[x + w], [y + h], [z]]
        ]

        # 切り取る画像のサイズ（画素）
        # self._clipped_width, self._clipped_height = image_size
        self._clipped_width = image_size
        self._clipped_height = int(self._clipped_width * h / w)

        # 検出するマーカーボード（基準座標とする予定）の設定
        self._markers_x = 6  # マーカーボードのx方向に配置したマーカー数
        self._markers_y = 1  # マーカーボードのy方向に配置するマーカー数
        self._marker_length = 0.03  # マーカーのサイズ 画素数ではない。メータ指定
        self._marker_separation = 0.005  # マーカー間の間隔
        self._first_marker_id = 100  # 検出するマーカーボードの一番最初のマーカのidを指定

        # arucoライブラリ
        self._aruco = cv2.aruco
        # 使用するARマーカーのdictionary
        self._dictionary = self._aruco.getPredefinedDictionary(self._aruco.DICT_4X4_250)
        # 検出するマーカーボード
        self._grid_board = self._aruco.GridBoard_create(
            self._markers_x,
            self._markers_y,
            self._marker_length,
            self._marker_separation,
            self._dictionary,
            self._first_marker_id
        )
        # aruco.detectMarkers()実行時に渡すパラメータ（必須ではない？？）
        self._parameters = self._aruco.DetectorParameters_create()
        # CORNER_REFINE_NONE, no refinement.
        # CORNER_REFINE_SUBPIX, do subpixel refinement.
        # CORNER_REFINE_CONTOUR use contour-Points
        self._parameters.cornerRefinementMethod = self._aruco.CORNER_REFINE_CONTOUR

        # カメラパラメータの読み込み
        self._camera_matrix = np.asarray(load_parameter(cam_matrix_file))
        self._camera_dist = np.asarray(load_parameter(cam_distortion_file))

    def estimate_board(self, gray: np.ndarray):
        """
        マーカーボードを検出し、そのrotation vectorとtranslation vectorを返す
        :param gray: opencvでキャプチャした画像(グレースケール)データ
        :return:
        """
        # とりあえずマーカー類を全部個別に検出
        corners, ids, rejected_ImgPoints = self._aruco.detectMarkers(
            gray,
            self._dictionary,
            parameters=self._parameters
        )
        # print(rejected_ImgPoints) # 正しく検出できなかったマーカーの情報？デバッグ目的

        # 検出した全マーカーを対象として、特定のーカーボードの位置推定
        # opencv4以降引数が増えている
        # https://docs.opencv.org/4.1.1/d9/d6a/group__aruco.html#ga366993d29fdddd995fba8c2e6ca811ea
        retval, rvec_org, tvec_org = self._aruco.estimatePoseBoard(
            corners,
            ids,
            self._grid_board,
            self._camera_matrix,
            self._camera_dist,
            None,
            None
        )

        return retval, rvec_org, tvec_org

    def roi_coordinates(self, rvec_org, tvec_org):
        """
        clippingする範囲の画像座標（ROI）を取得する
        :param rvec_org: rotation vector
        :param tvec_org: translation vector
        :return: Region of Interest 切り取り範囲（4端点：画像座標系）
        """
        rvec_o = cv2.Rodrigues(rvec_org)
        rvec_o = rvec_o[0]

        # 切り取り範囲(マーカー座標系)をカメラ座標系の座標に変換
        cut_area_cam = [rvec_o @ np.array(m) + tvec_org for m in self._clipping_area]

        # カメラ座標を画像座標に変換
        roi = [camera2screen(self._camera_matrix, c) for c in cut_area_cam]

        return roi

    def image_clipping(self, img: np.ndarray, roi: Sequence[Union[int, float]]) -> np.ndarray:
        """
        :param img:
        :param roi: Region of Interest 切り取り範囲（4端点：画像座標系）
        :return: roiを切り取って、指定したサイズの矩形に投影した画像
        """
        # 画像を切り取る
        roi_coordinates = np.float32(roi)
        true_coordinates = np.float32([
            [0., 0.],
            [self._clipped_width, 0.],
            [0., self._clipped_height],
            [self._clipped_width, self._clipped_height]
        ])
        trans_mat = cv2.getPerspectiveTransform(roi_coordinates, true_coordinates)
        img_trans = cv2.warpPerspective(img, trans_mat, (self._clipped_width, self._clipped_height))
        return img_trans

    def draw_clipping_frame(self, img: np.ndarray, roi: Sequence[Union[int, float]], rvec_org, tvec_org):
        """
        画像に原点軸やクリッピングする枠など描画する
        :param img: 描画対象となるopencvでキャプチャした画像
        :param roi: Region of Interest 切り取り範囲（4端点：画像座標系）
        :param rvec_org:
        :param tvec_org:
        :return:
        """
        # 検出したマーカーボードの原点座標を描画する
        axis_length = 0.1
        self._aruco.drawAxis(img, self._camera_matrix, self._camera_dist,
                             rvec_org, tvec_org, axis_length)

        # convexHullで輪郭の座標を順番通りに取得して、元画像に枠を描画
        roi_int = np.array(roi, dtype=np.int32)
        hull = cv2.convexHull(roi_int, returnPoints=True)
        cv2.drawContours(img, [hull], -1, (0, 255, 255), 2)  # 「-1」は画像中の全輪郭を描画する


def main():
    # 検出したマーカーボード基準で画像の切り取りを行うクラスのインスタンスを生成
    clipper = ArClipper()

    size = (1920, 1080)
    # size = (1280, 720)

    cap = cv2.VideoCapture(0)
    # cap = cv2.VideoCapture(0 + cv2.CAP_DSHOW)  # Windows:こうしないとエラーになる場合がある
    # cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # Windows:こうしないとエラーになる場合がある

    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, size[1])
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, size[0])
    print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    print(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    if cap.isOpened() is False:
        raise ("IO Error")

    while True:
        ret, img = cap.read()
        # if ret == False:
        if ret is False:
            continue

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # マーカーボードを検出し、そのrotation vectorとtranslation vectorを返す
        retval, rvec_org, tvec_org = clipper.estimate_board(gray)

        if retval != 0:  #
            # clippingする範囲の画像座標（ROI）を取得する
            roi = clipper.roi_coordinates(rvec_org, tvec_org)

            # 画像を切り取る
            cliiped_img = clipper.image_clipping(img, roi)

            # cliiped_imgに対する何等かの処理をここで行う

            # 画像に原点軸やクリッピングする枠など描画する
            clipper.draw_clipping_frame(img, roi, rvec_org, tvec_org)

        cv2.imshow("Image", img)

        # Escキーで終了
        key = cv2.waitKey(50)
        if key == 27:  # ESC
            break

    print("finish")
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
