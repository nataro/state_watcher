from abc import ABCMeta, abstractmethod

from typing import List, Tuple, Dict, Optional
import collections


class BaseState(metaclass=ABCMeta):
    def __init__(self, state_key: str = ''):
        self._state_key = state_key

    @property
    def state_key(self):
        return self._state_key

    @abstractmethod
    def state_transition(self):
        pass


class StateNoWorkpiece(BaseState):
    def state_transition(self, context):
        if context.state.__class__ is not self.__class__:
            return self


class StateNewWorkpiece(BaseState):
    def state_transition(self, context):
        if context.state.__class__ is not self.__class__:
            return self


class StateProcessing(BaseState):
    def state_transition(self, context):
        if context.state.__class__ is not self.__class__:
            return self


class StateSetup(BaseState):
    def state_transition(self, context):
        if context.state.__class__ is not self.__class__:
            return self


class StateContext(object):
    # 状態のインスタンスを辞書型で保持させておく
    # 都度インスタンスを生成するとIDが変わるので都合が悪い
    state_list = ['00_nowork', '01_newwork', '02_processing', '03_handling']
    state_dict = collections.defaultdict(BaseState)
    state_dict[state_list[0]] = StateNoWorkpiece()
    state_dict[state_list[1]] = StateNewWorkpiece()
    state_dict[state_list[2]] = StateProcessing()
    state_dict[state_list[3]] = StateSetup()

    def __init__(self, buffer_num: int = 5):
        # リングバッファにBaseStateクラスを格納していく予定
        self._state_deque = collections.deque(maxlen=buffer_num)
        self._state = list(StateContext.state_dict.values())[0]  # 現在の状態
        self._state_deque.append(self._state)

    @property
    def state_keys(self):
        """このクラスで定義されている状態を示す辞書のkeyリストを得る"""
        return list(StateContext.state_dict.keys())

    def feed_state_info(self, state_key: str):
        """
        このクラスで定義されている状態を示す辞書のkeyを文字列で渡し、
        直近の状態を保持するリングバッファに登録する
        """
        state_obj = StateContext.state_dict.get(state_key, None)
        if state_obj:
            self._state_deque.append(state_obj)

    @property
    def state(self) -> BaseState:
        """
        現時点における状態をBaseStateクラスで得る
        """
        return self._state

    def state_transition(self) -> Optional[str]:
        """
        リングバッファに登録されている情報を確認し、投票結果から現在の状態を推定する。
        その際、更新前にself._stateに格納されている状態から変化があった場合にのみ、
        その状態のkeyを文字列で返し、それ以外はNoneを返すようにしている

        基本的に、feed_state_info()で情報を与えたのち、セットでstate_transition()を実行して
        状態の遷移とその確認を実施するような運用になる
        """
        new_state = self.check_state_mode()
        # 以前の状態から変化があった場合は、変化後の状態を示すBaseStateクラスが返される
        changed_state = new_state.state_transition(self)
        self._state = new_state

        if changed_state:
            # 状態に変化があった場合は、その状態を示すKeyを返す
            return self.get_state_key(changed_state)

    def check_state_mode(self) -> BaseState:
        # 直近の最頻値のStateを返す
        c = collections.Counter(self._state_deque).most_common()
        mode_state = c[0][0]
        return mode_state

    def get_state_key(self, state: BaseState) -> Optional[str]:
        # 一致するキーのリストを得る
        keys = [k for k, v in StateContext.state_dict.items() if v == state]

        if keys:
            # キーのリストのうち1個目を返す
            return keys[0]


if __name__ == "__main__":
    import time

    context = StateContext(buffer_num=5)
    keys = context.state_keys
    print(keys)

    # context.feed_state_info('hogehoge')
    # print(context.state_transition())

    context.feed_state_info('00_nowork')
    print(context.state_transition())

    context.feed_state_info('01_newwork')
    print(context.state_transition())

    context.feed_state_info('01_newwork')
    print(context.state_transition())

    context.feed_state_info('01_newwork')
    print(context.state_transition())

    context.feed_state_info('02_processing')
    print(context.state_transition())

    context.feed_state_info('02_processing')
    print(context.state_transition())

    context.feed_state_info('02_processing')
    print(context.state_transition())

    context.feed_state_info('02_processing')
    print(context.state_transition())
